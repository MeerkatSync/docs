# Folders view

This time we will make view containing all folders with notes. Open *FoldersView.swift* file.

## Observing all `#!swift NotesFolder`

Objects derived from `SyncObject` can be observed as standalone object or as collection of them. To get observable over all valid objects just use `#!swift observableObjects` property.

Replace `folders` property with following:

```swift
@ObservedObject
private var folders = NotesFolder.observableObjects

private var sortedFolders: [SyncObjectWrapper<NotesFolder>] {
    folders.elements.sorted { $0.title < $1.title }
}
```

When any valid `#!swift NotesFolder` object is updated, your view will be automatically refreshed. `sortedFolders` property sorts all folders alphabetically.

!!! info "`#!swift SyncObjectWrapper<T>` class"

    Is generic class used to wrap any object deriving from `SyncObject`. This object has localy stored id so you can access it even when object is deleted. You can access all property of wrapped object (`T`) same way is if it was not wrapped. 

Now update ForEach from:

```swift
ForEach(folders) { folder in
    Text("folder")
}
```

to:

```swift
ForEach(sortedFolders) { folder in
    HStack {
        Text(folder.title)
        Spacer()
        Text("(\(folder.notes.count))")
            .foregroundColor(.secondary)
    }
}
```

so you app can compile.

## Add new folder

To see that folders are observed we need to be able to add new folder. Open *NewFolderView.swift* and add following code before `#!swift present.wrappedValue.dismiss()` in `#!swift save()` function.

```swift
// 1
let realm = getRealm()

// 2
try! realm.write {
    // 3
    let newGroup = SyncGroup()
    realm.add(newGroup)

    // 4
    let folder = NotesFolder(title: title)
    folder.group = newGroup
    realm.add(folder)
}
```

1. Get a `Realm` instance.
2. Write transaction to the database.
3. Create a new group and store it to the database.
4. Create a new folder and store it to the database.

!!! question "Why new group?"

    We create new group for every folder to make sharing really simple. When we will want to share folder with somebody we just add then to the group.
    
At this time your app can store new folders. Build & run your app and create few folders (use navigation button). Then log out from app, log in as another user and create another folders. Finaly log out and log in as first user. Every user see only his folders.

## Delete folder

To delete a folder open *FoldersView.swift* and insert following code into body of `deleteFolders(indexes:)` function:

```swift 
let realm = getRealm()
try! realm.write {
    // 1
    let foldersToDelete = indexes.map { sortedFolders[$0] }
    foldersToDelete.forEach { folder in
        // 2
        folder.ifValid { folder in
            // 3
            folder.notes.forEach {
                $0.isDeleted = true
            }
            // 4
            folder.isDeleted = true
            // 5
            folder.group?.isDeleted = true
        }
    }
}
```

1. Get all removed folders.
2. If folder is valid and not deleted call closure
3. Set all notes in folder as deleted.
4. Set folder as deleted.
5. Set group of folder as deleted.

!!! important "Can I delete an object with `#!swift realm.delete(_:)`?"

    Not if object is used in synchronization process. In that case you have to use soft delete as follows:
    
    ```swift
    object.isDeleted = true
    ```
    
To delete folder just slide row with folder from right to left :)

## Summary

From this time you can see that every change is synchronized between all user's devices. Try run multiple simulators, log in with same user and enjoy synchronization. Every update you made should be in 30 seconds[^1] synchronized on all devices. When you make any update all previous updates are downloaded after synchronization.

In the next page we make possible to add new notes to folders and learn how to handle when currently viewed object is deleted or modified. :)

??? note "Content of *FoldersView.swift*"

    ```swift
    import SwiftUI
    import MeerkatRealmWrapper
    
    struct FoldersView: View {
    
        @State
        private var activeFolder: String? = nil
    
        @ObservedObject
        private var folders = NotesFolder.observableObjects
    
        private var sortedFolders: [SyncObjectWrapper<NotesFolder>] {
            folders.elements.sorted { $0.title < $1.title }
        }
    
        @State
        private var newFolder = false
    
        var body: some View {
            NavigationView {
                List {
                    ForEach(sortedFolders) { folder in
                        HStack {
                            Text(folder.title)
                            Spacer()
                            Text("(\(folder.notes.count))")
                                .foregroundColor(.secondary)
                        }
                    }
                    .onDelete(perform: deleteFolders)
                }.navigationBarTitle("Folders")
                .navigationBarItems(trailing: Button(action: {
                    self.newFolder = true
                }) {
                    Image(systemName: "folder.badge.plus")
                })
                    .sheet(isPresented: $newFolder) {
                        NewFolderView()
                }
            }
        }
    
        func deleteFolders(indexes: IndexSet) {
            let realm = getRealm()
            try! realm.write {
                // 1
                let foldersToDelete = indexes.map { sortedFolders[$0] }
                foldersToDelete.forEach { folder in
                    // 2
                    folder.ifValid { folder in
                        // 3
                        folder.notes.forEach {
                            $0.isDeleted = true
                        }
                        // 4
                        folder.isDeleted = true
                        // 5
                        folder.group?.isDeleted = true
                    }
                }
            }
        }
    }
    
    struct FoldersView_Previews: PreviewProvider {
        static var previews: some View {
            FoldersView()
        }
    }
    ```
    

??? "Content of *NewFolderView.swift*"

    ```swift
    import SwiftUI
    import MeerkatRealmWrapper
    
    struct NewFolderView: View {
        @Environment(\.presentationMode) var present
    
        @State
        private var title: String = ""
    
        @State
        private var invalidTitle = false
    
        var body: some View {
            NavigationView {
                VStack {
                    TextField("Title", text: $title)
                        .modifier(FormRowModifier())
                    Button(action: save) {
                        Text("Save")
                    }.modifier(FormButtonModifier(color: .blue))
                    Spacer()
                }.padding()
                .navigationBarTitle("New folder")
            }
                .alert(isPresented: $invalidTitle) {
                Alert(title: Text("Title is ivalid"))
            }
        }
    
        private func save() {
            // 1
            let realm = getRealm()
    
            // 2
            try! realm.write {
                // 3
                let newGroup = SyncGroup()
                realm.add(newGroup)
    
                // 4
                let folder = NotesFolder(title: title)
                folder.group = newGroup
                realm.add(folder)
            }
    
            present.wrappedValue.dismiss()
        }
    }
    
    struct NewFolderView_Previews: PreviewProvider {
        static var previews: some View {
            NewFolderView()
        }
    }
    ```

    
[^1]: This time is based on refresh duration in your timer. If you change timer interval to lower number your app will be pulling changes more often.
