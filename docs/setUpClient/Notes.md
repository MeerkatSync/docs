# Notes view and handling object change

With synchronization comes some problems. For example imagine you are editing note but some other user modify it too. Or even worst somebody deletes this note. Your app should respond to this action properly. In this page we prepare our app for some of these problems.

## Folder detail

Let's start with folder detail. Open *FolderDetailView.swift* and add following code into the *FolderDetailView*:

```swift
// 1
@ObservedObject
private var folder: ObservableSyncObject<NotesFolder>

// 2
init(folder: ObservableSyncObject<NotesFolder>) {
    self.folder = folder
}

// 3
#if DEBUG
init() {
    folder = ObservableSyncObject()
}
#endif
```

1. Object observing given object.
2. Init used by different view.
3. Init used in preview. 

!!! note "Empty initializer of `ObservableSyncObject`"

    We can observe only objects stored in database. If you don't want to observe any object you can use empty initialized. This is good for UI previews or in scenarios where you sometimes have object and sometimes not (for example if you use same view for creating new and editing object).
    
## Present FolderDetailView

Open *FoldersView.swift* and add following function to view:

```swift
private func folderLink(for folder: SyncObjectWrapper<NotesFolder>) -> some View {
    // 1
    let observableFolder = folder.observableObject()
    // 2
    let destination = FolderDetailView(folder: observableFolder)
    let view = NavigationLink(
        destination: destination,
        // 3
        tag: folder.id,
        // 4
        selection: self.$activeFolder) {
            // 5
            HStack {
                Text(folder.title)
                Spacer()
                Text("(\(folder.notes.count))")
                    .foregroundColor(.secondary)
            }
    }
    return view
}
```

1. Create object observing wrapped folder.
2. Create destination view.
3. Set tag for destination view. We are using `id` property because it is unique.
4. Set state property holding currently previewed folder.
5. View for a row with folder in table.

Replace content of `#!swift ForEach(...) { ... }` with following:

```swift
ForEach(sortedFolders) { folder in
    self.folderLink(for: folder)
}
```

From this time a view with the folder detail is showed when user taps the folder's row.

## Handling folder deletion

If app is presenting folder detail and this folder is removed our app needs to react. Let's say we want to close this view so user can't interact with deleted folder. 

Replace `#!swift let observableFolder = folder.observableObject()` in `#!swift folderLink(for:)` function with following:

```swift
let observableFolder = folder.observableObject().onDelete {
    self.activeFolder = nil
}
```

Because created observable object is stored only in the view we want to close we can add closure which will be called if observable object is not deinitialized and observed object is removed or does not exist.

!!! info "`onDelete` closure and strong reference."

    You can use strong reference in this closure because closure is not stored within observable object.
    
Build & run your app with multiple devices. In one device open folder detail and in another delete this folder. Wait until synchronization is complete. Your app should close detail view as soon as it detects that presenting folder is removed.
    
## Show notes in a folder

Now when we are observing a folder we can show to user all notes in it. Open *FolderDetailView.swift*  and replace `#!swift let notes: [Tmp] = []` with:

```swift
private var notes: [SyncObjectWrapper<Note>] {
    // 1
    folder.transform(defaultValue: []) { folder in
        // 2
        folder.notes.sorted(byKeyPath: \.created, ascending: false).wrappedArray
    }
}
```

1. Every `ObservableSyncObject` provides easy method to access observed object. In this case we use `transform(defaultValue:_:)` function which return `defaultValue` if object is deleted or invalidated and returns result of closure otherwise.
2. Get all notes sorted by creation date.

Now replace button's label `#!swift Text("Note")` with following:

```swift
Text(note.title)
```

## Presenting NoteView

In this project we use same view for creating new note and for editing existing note - `#!swift NoteView`.This view is presented as sheet. Edit `#!swift case editNote(String)` in `SheetType` enum to:

```swift 
case editNote(SyncObjectWrapper<Note>)
```

in the same button as before edit action from `#!swift self.isEditing = .editNote("")` to:

```swift
self.isEditing = .editNote(note)
```

## Create a new note

When creating a new note we need to know associated folder. Open *NoteView.swift* and replace whole `init()` with following:

```swift
private let folder: ObservableSyncObject<NotesFolder>

init(folder: ObservableSyncObject<NotesFolder>) {
    self.folder = folder
    isEditing = false
    _title = .init(wrappedValue: "")
    _text = .init(wrappedValue: "")
}
```

`folder` property holds observable folder associated with new note.


### Store note

There is already function that is called when user tries to create new note. In the `#!swift saveNewNote()` function add following code:

```swift
// 1
folder.ifValid { folder in
    let realm = getRealm()
    try! realm.write {
        // 2
        let note = Note(title: title, text: text)
        // 3
        note.group = folder.group
        // 4
        realm.add(note)
        // 5
        folder.notes.append(note)
    }
}
```

1. If folder is not removed or invalidated do following closure.
2. Creates new folder.
3. Set group of the note to the same group as associated folder
4. Store note to the database.
5. Add note into the folder.

### Fix compilation errors

In preview structure `NoteView_Previews` update `NoteView()` to:

```swift
NoteView(folder: .init())
```

In *FolderDetailView.swift* replace `view(close:)` function in `enum SheetType` with following:

```swift
func view(folder: ObservableSyncObject<NotesFolder>, close: @escaping () -> Void) -> some View {
    switch self {
    case .addNote:
        return AnyView(NoteView(folder: folder))
    case .share:
        return AnyView(ShareView(group: .init()))
    case .editNote(let n):
        return AnyView(NoteView(folder: folder))
    }
}
```

and it's calling in `#!swift .sheet` modifier from `#!swift sheet.view()` to:

```swift
sheet.view(folder: self.folder)
```

Build & run and add some new notes.

## Edit a note

As mentioned we use `NoteView` for editing too. Open *NoteView.swift*.

### Init with note

Add new initialized for `NoteView` as follows:

```swift
@ObservedObject
private var note: ObservableSyncObject<Note>

init(note: ObservableSyncObject<Note>, folder: ObservableSyncObject<NotesFolder>) {
    self.note = note
    self.folder = folder
    isEditing = true
    _title = .init(wrappedValue: note.transform { $0?.title } ?? "")
    _text = .init(wrappedValue: note.transform { $0?.text } ?? "")
}
```

This code adds new property - `note`. This property must be initialized in `init(folder:)` too so add following code in it:

```swift
note = .init()
```

### Present NoteView for editing and handle deletion

Lets say we want to handle deletion of note same way as deletion of its folder. If note is deleted we disappear the view. Open *FolderDetailView.swift* and at the begining of the `#!swift view(folder:close)` function add following code:


```swift
// 1
let folderObservableCopy = folder.observableCopy
// 2
let folder = folderObservableCopy.onDelete(call: close)
```

1. Create new observable copy so new all `onDelete` closures are associated with this copy and removed when copy is deinitialized.
2. Add `onDelete` closure for folder.

Now in the same function (`#!swift view(folder:close`) replace return value for `#!swift case .editNote(let n):` to following:

```swift
let note = n.observableObject().onDelete(call: close)
return AnyView(NoteView(note: note, folder: folder))
```

This add `onDelete` closure for the note too.

### Update note

Now open *NoteView.swift* again and insert following code into the `update()` function:

```swift
note.ifValid { note in
    let realm = getRealm()
    try! realm.write {
        note.title = title
        note.text = text
    }
}
```

From this time your app can add a new note or edit existing notes.

### Delete note

In *FolderDetailView.swift* set body of `deleteNotes(indexes:)` function to following:

```swift
// 1
let notesToDelete = indexes.map { notes[$0] }
let realm = getRealm()
try! realm.write {
    notesToDelete.forEach { note in
        // 2
        note.ifValid { note in
            note.isDeleted = true
        }
    }
}
```

1. Get all notes marked as deleted
2. Set note as deleted if note is valid.

Build and run you app and check if everything is working fine. When you delete opened note all views should close after this update is propagated to the device. But if you have opened note which is updated on different device you will not see any updates until you reopen this note.

## Update note view on change

Let's say we want to alert user about modified note and ask him if he want's to update note to its synchronized version or save its current version. Open *NoteView.swift* once again.

### Catch updates

You can set function to be called whenever given publisher published value. We can use `note`'s publisher to catch updates. Add following code at the end of `NavigationView { ... }`

```swift
.onReceive(note.objectWillChange, perform: didChange)
```

### Handle updates

We are interested in update only if it updates `title` or `text` property. Add following right at the begining of `didChange()` function:

```swift
note.ifValid { note in
    guard text != note.text || title != note.title else {
        return
    }
    DispatchQueue.main.async {
        self.objectChanged = true
    }
}
```

Now when title and text visible to user is not same as title and text stored in note we open an alert notifying user about situation.

### Reload

Resave of note is already implemented. Now we need to implement reload function. Add following code in the body of the `reload()` function:

```swift
note.ifValid { note in
    title = note.title
    text = note.text
}
```

## Summary

Build & run and enjoy your work. We implemented views for notes and define how to handle deletion and modification of objects in view. The last part is to enable sharing of folders between multiple users.


??? note "Contents of *FolderDetailView.swift*"
    ```swift
    import SwiftUI
    import MeerkatRealmWrapper
    
    struct FolderDetailView: View {
    
        @State
        private var isEditing: SheetType? = nil
    
        // 1
        @ObservedObject
        private var folder: ObservableSyncObject<NotesFolder>
    
        // 2
        init(folder: ObservableSyncObject<NotesFolder>) {
            self.folder = folder
        }
    
        // 3
        #if DEBUG
        init() {
            folder = ObservableSyncObject()
        }
        #endif
    
        private var notes: [SyncObjectWrapper<Note>] {
            // 1
            folder.transform(defaultValue: []) { folder in
                // 2
                folder.notes.sorted(byKeyPath: \.created, ascending: false).wrappedArray
            }
        }
    
        var body: some View {
            VStack {
                List {
                    ForEach(notes, id: \.id) { note in
                        Button(action: {
                            self.isEditing = .editNote(note)
                        }) {
                            Text(note.title)
                        }
                    }.onDelete(perform: deleteNotes)
                }.navigationBarTitle("Folder")
                    .navigationBarItems(trailing: HStack {
                        Button(action: {
                            self.isEditing = .share
                        }) {
                            Image(systemName: "person.3")
                        }.padding(.trailing)
                        Button(action: {
                            self.isEditing = .addNote
                        }) {
                            Image(systemName: "plus")
                        }
                    })
    
            }.sheet(item: $isEditing) { sheet in
                sheet.view(folder: self.folder) {
                    DispatchQueue.main.async {
                        self.isEditing = nil
                    }
                }
            }
        }
    
        func deleteNotes(indexes: IndexSet) {
            // 1
            let notesToDelete = indexes.map { notes[$0] }
            let realm = getRealm()
            try! realm.write {
                notesToDelete.forEach { note in
                    // 2
                    note.ifValid { note in
                        note.isDeleted = true
                    }
                }
            }
        }
    }
    
    extension FolderDetailView {
        enum SheetType: Identifiable {
            var id: ObjectIdentifier { .init(SheetType.self) }
    
            case addNote
            case share
            case editNote(SyncObjectWrapper<Note>)
    
            func view(folder: ObservableSyncObject<NotesFolder>, close: @escaping () -> Void) -> some View {
                // 1
                let folderObservableCopy = folder.observableCopy
                // 2
                let folder = folderObservableCopy.onDelete(call: close)
                switch self {
                case .addNote:
                    return AnyView(NoteView(folder: folder))
                case .share:
                    return AnyView(ShareView(group: .init()))
                case .editNote(let n):
                    let note = n.observableObject().onDelete(call: close)
                    return AnyView(NoteView(note: note, folder: folder))
                }
            }
        }
    }
    
    struct FolderDetail_Previews: PreviewProvider {
        static var previews: some View {
            NavigationView {
                FolderDetailView()
            }
        }
    }
    ```

??? note "Contents of *FoldersView.swift*"

    ```swift
    import SwiftUI
    import MeerkatRealmWrapper
    
    struct FoldersView: View {
    
        @State
        private var activeFolder: String? = nil
    
        @ObservedObject
        private var folders = NotesFolder.observableObjects
    
        private var sortedFolders: [SyncObjectWrapper<NotesFolder>] {
            folders.elements.sorted { $0.title < $1.title }
        }
    
        @State
        private var newFolder = false
    
        private func folderLink(for folder: SyncObjectWrapper<NotesFolder>) -> some View {
            // 1
            let observableFolder = folder.observableObject().onDelete {
                self.activeFolder = nil
            }
            // 2
            let destination = FolderDetailView(folder: observableFolder)
            let view = NavigationLink(
                destination: destination,
                // 3
                tag: folder.id,
                // 4
                selection: self.$activeFolder) {
                    // 5
                    HStack {
                        Text(folder.title)
                        Spacer()
                        Text("(\(folder.notes.count))")
                            .foregroundColor(.secondary)
                    }
            }
            return view
        }
    
        var body: some View {
            NavigationView {
                List {
                    ForEach(sortedFolders) { folder in
                        self.folderLink(for: folder)
                    }
                    .onDelete(perform: deleteFolders)
                }.navigationBarTitle("Folders")
                .navigationBarItems(trailing: Button(action: {
                    self.newFolder = true
                }) {
                    Image(systemName: "folder.badge.plus")
                })
                    .sheet(isPresented: $newFolder) {
                        NewFolderView()
                }
            }
        }
    
        func deleteFolders(indexes: IndexSet) {
            let realm = getRealm()
            try! realm.write {
                // 1
                let foldersToDelete = indexes.map { sortedFolders[$0] }
                foldersToDelete.forEach { folder in
                    // 2
                    folder.ifValid { folder in
                        // 3
                        folder.notes.forEach {
                            $0.isDeleted = true
                        }
                        // 4
                        folder.isDeleted = true
                        // 5
                        folder.group?.isDeleted = true
                    }
                }
            }
        }
    }
    
    struct FoldersView_Previews: PreviewProvider {
        static var previews: some View {
            FoldersView()
        }
    }
    ```
    
??? note "Contents of *NoteView.swift*"

    ```swift
    import SwiftUI
    import MeerkatRealmWrapper
    
    struct NoteView: View {
        @Environment(\.presentationMode) var present
    
        private let folder: ObservableSyncObject<NotesFolder>
    
        @ObservedObject
        private var note: ObservableSyncObject<Note>
    
        init(note: ObservableSyncObject<Note>, folder: ObservableSyncObject<NotesFolder>) {
            self.note = note
            self.folder = folder
            isEditing = true
            _title = .init(wrappedValue: note.transform { $0?.title } ?? "")
            _text = .init(wrappedValue: note.transform { $0?.text } ?? "")
        }
    
        init(folder: ObservableSyncObject<NotesFolder>) {
            self.folder = folder
            isEditing = false
            _title = .init(wrappedValue: "")
            _text = .init(wrappedValue: "")
            note = .init()
        }
    
        let isEditing: Bool
        @State
        private var objectChanged: Bool = false
    
        @State
        private var title: String
    
        @State
        private var text: String
    
        var body: some View {
            NavigationView {
                VStack {
                    TextField("Title", text: $title)
                        .modifier(FormRowModifier())
                    TextField("text", text: $text)
                        .modifier(FormRowModifier())
                    Button(action: save) {
                        Text("Save")
                    }.modifier(FormButtonModifier(color: .green))
                    Spacer()
                }.padding()
                .alert(isPresented: $objectChanged) {
                    Alert(title: Text("Note is updated!"),
                          message: Text("There is updated version of note. Do you want to reload?"),
                          primaryButton: .destructive(Text("Reload"), action: reload),
                          secondaryButton: .cancel(Text("Save current version"), action: save))
                }.navigationBarTitle(isEditing ? title : "New note")
            }.onReceive(note.objectWillChange, perform: didChange)
        }
    
        func didChange() {
            note.ifValid { note in
                guard text != note.text || title != note.title else {
                    return
                }
                DispatchQueue.main.async {
                    self.objectChanged = true
                }
            }
        }
    
        func reload() {
            note.ifValid { note in
                title = note.title
                text = note.text
            }
        }
    
        func save() {
            if isEditing {
                update()
            } else {
                saveNewNote()
            }
            close()
        }
    
        func update() {
            note.ifValid { note in
                let realm = getRealm()
                try! realm.write {
                    note.title = title
                    note.text = text
                }
            }
        }
    
        func saveNewNote() {
            // 1
            folder.ifValid { folder in
                let realm = getRealm()
                try! realm.write {
                    // 2
                    let note = Note(title: title, text: text)
                    // 3
                    note.group = folder.group
                    // 4
                    realm.add(note)
                    // 5
                    folder.notes.append(note)
                }
            }
        }
    
        func closeAlert() {
            objectChanged = false
        }
    
        func close() {
            self.present.wrappedValue.dismiss()
        }
    }
    
    struct NoteView_Previews: PreviewProvider {
        static var previews: some View {
            NoteView(folder: .init())
        }
    }    
    ```

