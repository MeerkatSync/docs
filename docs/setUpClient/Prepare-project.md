# Prepare a project

In this tutorial we will cover how to set up synchronization in iOS app with the server created in previous tutorial :) 

!!! important "Prerequisites"

    To fully understand this tutorial, you should be familiar with iOS app programming, SwiftUI and Realm libraries.
    
??? info "Completed project from this tutorial"

    You can download completed project from its [repository](https://gitlab.com/MeerkatSync/templates/clienttutorialcompleted).

## Download template

Clone [template](https://gitlab.com/MeerkatSync/templates/clienttutorialtemplate) for a client and open it in Xcode. Hit ++cmd+b++ to build your project. If your project is built successfully, you are ready to continue.

After successful build try to run iOS app with play button in Xcode (shortcut is ++cmd+r++) and take a look. There are some views but nothing is really working at this time but we make it work in short time ;).

!!! note "Project dependencies"

    This app is using two main dependencies:
    
    - [MeerkatClientSyncController](https://gitlab.com/MeerkatSync/MeerkatClientSyncControler): Synchronization process control and communication with the server.
    - [MeerkatRealmWrapper](https://gitlab.com/MeerkatSync/MeekratRealmWrapper): Wrapper over Realm database.

## What's inside my project?

First you need to get familiar with project structure to get some orientation in your new project.

### LogInView

View used to log in or registration.

### Constants

Contains configuration used to connect to the server.

### SyncAuthStatus

Observable object used to keep authentication status in property `authStatus`. Value of this property is automatically stored in `UserDefaults`.

## Structures used in the server

In the server we implement structures used in registration and protocols for our database scheme. Copy following files (or create new with same content) from the server to this project in *Model* directory:

- *Models/PublicUserInformation.swift*: remove imports and replace `Content` with `#!swift Codable`
- *Models/PublicGroupUser.swift*: replace `import MeerkatServerProvider` with `import MeerkatCore` and replace `Content` with `#!swift Codable`
- *Models/SyncScheme.swift*: this file is ok :)

!!! important "Run server"

    Don't forget to run you server app created in the previous tutorial :)
   

??? note "Content of *PublicUserInformation.swift*"

    ```swift
    struct PublicUserInformation: Codable {
        let username: String
        let password: String
    }
    ```

??? note "Content of *PublicGroupUser.swift*"

    ```swift
    import MeerkatCore
    
    struct PublicGroupUser: Codable {
        let username: String
        let role: Role
    }
    ```

??? note "Content of *SyncScheme.swift*"

    ```swift
    import MeerkatSchemeDescriptor
    import Foundation
    
    public protocol NoteScheme: SchemeDescribable {
        var title: String { get }
        var text: String { get }
        var created: Date { get }
    }
    
    public protocol NotesFolderScheme: SchemeDescribable {
        var title: String { get }
        var notes: Notes { get }
    
        associatedtype Notes: Collection where Notes.Element: NoteScheme
    }
    ```
