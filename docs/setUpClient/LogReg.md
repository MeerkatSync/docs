# Login and registration

To let users use synchronization, they need to register. Template project has already prepared view for this. Open file *Settings/LogInView.swift*.

## Client API

MeerkatSync provides client's API with default routes. This API can be used with the instance of `#!swift MeerkatClientAPI` object. Add following code:

```swift
@ObservedObject
private var client = MeerkatClientAPI(clientConfig: Constants.clientConfig)
```

This object has bindable property `#!swift isLoading` to indicate if there is any connection with the server. We can use this property with `#!swift LoadingView` to provide feedback to user. Replace `#!swift LoadingView(isShowing:  .constant(false), cancel: cancelTask)`

with:

```swift
LoadingView(isShowing: $client.isLoading, cancel: cancelTask)
```

## Registration

There is `#!swift register` function already called when a user taps registration button. After validations add following code:

```swift
// 1
let user = PublicUserInformation(username: username, password: password)
// 2
task = client.register(user: user).receive(on: DispatchQueue.main).sink(receiveCompletion: { completion in
    switch completion { 
    // 3
    case .failure(let err as BadRequest):
        self.error = .requestError(err)
    case .failure(let err):
        self.error = .other(err) 
    // 4
    case .finished:
        self.close()
    }
}) { auth in
    // 5
    self.syncStatus.authStatus = .loggedIn(auth: auth)
    // 6
    self.storedUsername = self.username
}
```

1. User information required for registration.
2. Create registration task.
3. Handle errors.
4. Task finished successfully, set view as inactive to disappear current view.
5. Parameter `auth` contains sync id and token returned in response from the server (syncId and token). We store response in `SyncStatus` object to update global state of our app.
6. Store username to user defaults so we can use it in another views and when user is relogging in.

Run your application and try to register a user. Registration should work, but users can't log out from the app.

## Log out

User can logout in `SettingsView`. There is `logout` function already but with empty body. Fill it with following:

```swift
authStatus.authStatus = .loggedOut
```

Now try to log out from the app. It works like a magic.

## Log in
Return back to `LogInView` and insert following code before the end of `login` function:

```swift
let user = PublicUserInformation(username: username, password: password)
task = client.login(user: user, idKeyPath: \.username).receive(on: DispatchQueue.main).sink(receiveCompletion: { completion in
    switch completion {
    case .failure(let err as BadRequest):
        self.error = .requestError(err)
    case .failure(let err):
        self.error = .other(err)
    case .finished:
        self.close()
    }
}) { auth in
    self.syncStatus.authStatus = .loggedIn(auth: auth)
    self.storedUsername = self.username
}
```

There is only one difference between this code and code used in registration. Instead of `#!swift client.register` we call `#!swift client.login` function. This time we had to specify keypath to the unique `#!swift String` value.

Run app again and try to log in.

## Show username for relog

We can provide user's username in LoginView stored from last log in if he is trying to log in again. Add following code after `#!swift LoadingView(...) { ... }`:

```swift
.onAppear {
    guard !self.isRegistration, let name = self.storedUsername else {
        return
    }
    self.username = name
}
```

This code is called right after your view appeared. We provide default falue for username if user is trying to log in and there is known username used in past.

## Summary

Users are able to register and log in to your application. In next page we set up synchronization process and finish `SettingsView`. :)

??? note "Content of *LogInView.swift*"

    ```swift
    import SwiftUI
    import MeerkatClientSyncController
    
    struct LogInView: View {
        let isRegistration: Bool
    
        let close: () -> Void
    
        @UserDefaultOptionalWrapper(.username)
        private var storedUsername: String?
    
        @ObservedObject
        private var syncStatus = SyncAuthStatus.shared
    
        @ObservedObject
        private var client = MeerkatClientAPI(clientConfig: Constants.clientConfig)
    
        @State private var username = ""
        @State private var password = ""
        @State private var password2 = ""
        @State private var task: Cancellable? = nil
    
        func login() {
            guard username.count >= 3 else {
                error = .shortUsername
                return
            }
            let user = PublicUserInformation(username: username, password: password)
            task = client.login(user: user, idKeyPath: \.username).receive(on: DispatchQueue.main).sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let err as BadRequest):
                    self.error = .requestError(err)
                case .failure(let err):
                    self.error = .other(err)
                case .finished:
                    self.close()
                }
            }) { auth in
                self.syncStatus.authStatus = .loggedIn(auth: auth)
                self.storedUsername = self.username
            }
        }
    
        func register() {
            guard username.count >= 3 else {
                error = .shortUsername
                return
            }
            guard password.count >= 8 else {
                error = .shortPassword
                return
            }
            guard password == password2 else {
                error = .passwordsMismatch
                return
            }
            // 1
            let user = PublicUserInformation(username: username, password: password)
            // 2
            task = client.register(user: user).receive(on: DispatchQueue.main).sink(receiveCompletion: { completion in
                switch completion {
                // 3
                case .failure(let err as BadRequest):
                    self.error = .requestError(err)
                case .failure(let err):
                    self.error = .other(err)
                // 4
                case .finished:
                    self.close()
                }
            }) { auth in
                // 5
                self.syncStatus.authStatus = .loggedIn(auth: auth)
                // 6
                self.storedUsername = self.username
            }
        }
    
        @State private var error: ReqError? = nil
    
        var body: some View {
            LoadingView(isShowing: $client.isLoading, cancel: cancelTask) {
                VStack {
                    TextField("Username", text: self.$username)
                        .modifier(FormRowModifier())
                    SecureField("Password", text: self.$password)
                        .modifier(FormRowModifier())
                    if self.isRegistration {
                        SecureField("Password again", text: self.$password2)
                            .modifier(FormRowModifier())
                    }
                    HStack {
                        Spacer()
                        if self.isRegistration {
                            Button(action: self.register) {
                                Text("Register")
                                    .modifier(FormButtonModifier(color: .orange))
                                    .padding(.top, 20)
                            }
    
                        } else {
                            Button(action: self.login) {
                                Text("Login")
                                    .modifier(FormButtonModifier(color: .green))
                                    .padding(.top, 20)
                            }
                        }
                        Spacer()
                    }
                    Spacer()
                }.padding().alert(item: self.$error) { (err) -> Alert in
                    Alert(title: Text(err.description))
                }
            }.onAppear {
                guard !self.isRegistration, let name = self.storedUsername else {
                    return
                }
                self.username = name
            }
        }
    
        func cancelTask() {
            task?.cancel()
        }
    }
    
    extension LogInView {
        private enum ReqError: Error, Identifiable, CustomStringConvertible {
            var id: String {
                description
            }
    
            var description: String {
                switch self {
                case .requestError(let r):
                    return "Error: Response status code: \(r.code), body: \(r.body)"
                case .passwordsMismatch:
                    return "Passwords are different"
                case .shortUsername:
                    return "Username is too short (at least 3 characters)"
                case .shortPassword:
                    return "Password is too short (at least 8 characters)"
                case .other(let err):
                    return "\(err)"
                }
            }
    
            case requestError(BadRequest)
            case passwordsMismatch
            case shortPassword
            case shortUsername
            case other(Error)
        }
    }
    
    struct LogInView_Previews: PreviewProvider {
        static var previews: some View {
            LogInView(isRegistration: true) { }
        }
    }
    ```
    
??? note "Content of *SettingsView.swift*"

    ```swift
    import SwiftUI
    
    struct SettingsView: View {
        @ObservedObject
        private var authStatus = SyncAuthStatus.shared
    
        @State
        private var selectedLink: NavLinkTag? = nil
    
        func logout() {
            authStatus.authStatus = .loggedOut
        }
    
        func resetApp() {
        }
    
        var body: some View {
            NavigationView {
                List {
                    Section {
                        if authStatus.isLoggedIn {
                            Button(action: logout, label: {
                                NavigationRow(text: "Log out", icon: "escape", iconColor: .orange)
                                .foregroundColor(.orange)
                            })
                        } else {
                            NavigationLink(destination: LogInView(isRegistration: false, close: returnToRoot), tag: .login, selection: $selectedLink) {
                                NavigationRow(text: "Log in", icon: "person", iconColor: .green)
                            }
    
                            NavigationLink(destination: LogInView(isRegistration: true, close: returnToRoot), tag: .registration, selection: $selectedLink) {
                                NavigationRow(text: "Registration", icon: "person.badge.plus", iconColor: .orange)
                            }
                        }
                    }
                    Section {
                        Button(action: resetApp, label: {
                            NavigationRow(text: "Reset app", icon: "trash", iconColor: .red)
                                .foregroundColor(.red)
                        })
                    }
                }.navigationBarTitle("Settings")
                    .listStyle(GroupedListStyle())
            }
        }
    
        func returnToRoot() {
            selectedLink = nil
        }
    }
    
    extension SettingsView {
        enum NavLinkTag {
            case login
            case registration
        }
    }
    
    struct SettingsView_Previews: PreviewProvider {
        static var previews: some View {
            SettingsView()
        }
    }
    ```
