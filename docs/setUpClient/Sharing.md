# Sharing

We design our app to be able to share folders between multiple users. Fot his purpose there is *ShareView.swift* file containing all important views but no functionality. To present this view tap icon with thee people next to the navigation title in `FolderDetailView`.

## Presenting ShareView

Open *FolderDetailView.swift* file and in `#!swift view(folder:close:)` function replace return value for `.share` case with following:

```swift
// 1
guard let group = folder.transform( { $0?.group?.observableObject() }) else {
    return AnyView(EmptyView())
}
// 2
return AnyView(ShareView(group: group.onDelete(call: close)))
```

1. Get group for the presenting folder. This should be never `nil` because folder must be in a group. Meaning of thi guard is just to be sure that our app wont crash.
2. Set `onDelete` closure for our group and present view.

## Show users

`ShareView` calls `reloadUsers` function right when its view appeared. In this function we reload users and present them to the user.

### Users property

We implement our server to return `PublicGroupUser` structure for every user. Replace `#!swift private let users: [Tmp] = []` property with:

```swift
@State
private var users = [PublicGroupUser]()
```

In this property we store same values as returned from server.

### Reload users

We use `MeerkatClientAPI` similar way as for registration to get all users in the group. `ShareView` has `client` property already so at the end of `reloadUsers()` function add following code:

```swift
// 1
group.ifValid { group in
    task = client
        // 2
        .getUsers(groupId: group.id, token: auth.authToken, userType: PublicGroupUser.self)
        // 3
        .sink(receiveCompletion: handleCompletion) { users in
            // 4
            self.users = users
    }
}
```

1. Check if group is still valid.
2. Get all users in the group. 
3. Add subscriber responsible to handle the task.
4. We successfully received users from the server.

!!! info "Using `PublicGroupUser`"

    We had to specify what data we expect for every user in server's response. 

### Present users

At this time there is small compilation error. Resolve it with replacing `#!swift ForEach(self.users) { ... }` with:

```swift
// 1
ForEach(self.users, id: \.username) { user in
    // 2
    UserShareDetail(username: user.username,
                    selectedRole: user.role)
}
```

1. Creates a row for every user (we need to use `id:` argument because `PublicGroupUser` does not comforms to `Identifiable`).
2. Use prepared view for a user.

From this time our view can present all users in the group. Build & run your app. You should see only currently logged user in the group because we didn't add any other user.

## Add user

To add a user to the group we need to known his sync id. Add following function:


```swift
func getSyncId(for userId: String) -> AnyPublisher<String, Error> {
    // 1
    guard let auth = authStatus.syncConfig, let user = user else {
        alertType = .notLoggedIn
        // 2
        return Empty().eraseToAnyPublisher()
    }
    // 3
    return client.syncIdFor(userId: userId, interestedUserId: user, token: auth.authToken)
}
```

1. Get authentication token and id of the group.
2. If the user is not logged in return empty publisher
3. Get sync id for user.

Now when we can get sync id for a new user add following function:

```swift
func add(userId: String, role: Role) {
    // 1
    guard let auth = getTokenWithGroupId() else {
        alertType = .notLoggedIn
        return
    }
    // 2
    let sub = getSyncId(for: userId).flatMap { syncId in
        // 3
        self.client.subscribe(syncId: syncId, to: auth.groupId, as: role, token: auth.token)
    }
    // 4
    task = taskWithReload(sub)
}
```

1. Get authentication token and id of the group.
2. Get sync id, then subscribe user for group updates.
3. Client's API to add user to the group.
4. Create task. `taskWithRelaod()` calls reload function when task is done so we can present all users in the group.

This function adds user with given id and role to the group. Add following code to body of the `add()` function.
 
```swift
add(userId: username, role: .readAndWrite)
username = ""
```

Now all new users in the group can read and write to the group.

## Group owner

Only owner can adds new users or change theirs role in the group. 

### Check role of the user

Every group holds `role` property with a role of the logged user. We can use this property to present only relevant views. Replace `private let isOwner = true` property with following:

```swift
private var isOwner: Bool {
    group.transform { $0?.role == .owner }
}
```

Views responsible for adding new users is, thank's to default implementation,  presented only for if logged user is owner of this group.

### Update users role

`UserShareDetail` user id `ForEach` can be initialized with 3 arguments. Third argument is closure called when the app user updates role. If third argument is `nil` (default value) it wont allow to update any role. Replace this view with following:

```swift
UserShareDetail(username: user.username,
                selectedRole: user.role,
                roleUpdated: self.isOwner ? self.add : nil)
```

Thanks to third parameter only owner of the group can update roles. 

!!! note
    
    Adding already added user updates his role

## Remove a user from group

We will remove users with `remove(indexes:)` function. After right `ForEach(...) { }` add 

```swift
.onDelete(perform: self.isOwner ? self.remove : nil)
```

to allow deletion only if the app users is owner of presented group.

### Remove users

Because SwiftUI API is designed to remove multiple rows at the same time we need to provide function removing multiple users. Add following code:

```swift
func remove(usersIds: [String]) {
    // 1
    guard let auth = getTokenWithGroupId() else {
        alertType = .notLoggedIn
        return
    }
    // 2
    let syncIds = usersIds.map { getSyncId(for: $0) }
    // 3
    let unsubs = syncIds.map { syncIdTask in
        syncIdTask.flatMap { syncId in
            self.client.unsubscribe(syncId: syncId, from: auth.groupId, token: auth.token)
        }
    }
    // 4
    let merged = mergePublishers(unsubs)
    // 5
    task = taskWithReload(merged)
}
```

1. Check if user is logged in.
2. Get sync id for users.
3. Create API calls removing users.
4. Merge multiple publishers into one.
5. Reload users when task is done.


### Check deleted users

If the app user tries to remove him self from the group we present alert so he can cancel action. Add following code:

```swift
func remove(indexes: IndexSet) {
    // 1
    guard let usersUsername = user else {
        alertType = .notLoggedIn
        return
    }
    // 2
    let usersIds = indexes.map { users[$0].username }
    // 3
    if usersIds.contains(usersUsername) {
        // 4
        alertType = .removeSelf({ self.remove(usersIds: usersIds) })
    } else {
        // 5
        remove(usersIds: usersIds)
    }
}
```

1. Check if user is logged in.
2. Get users.
3. Check if user is trying to remove him self too.
4. Aler the user about leaving the group.
5. Remove users

## Summary

Build & run your app. Your app is finished and you can fully enjoy it. Try to add users into the group or remove them. Everything should work just fine :)

??? note "Content of *FolderDetailView.swift*"

    ```swift
    import SwiftUI
    import MeerkatRealmWrapper
    
    struct FolderDetailView: View {
    
        @State
        private var isEditing: SheetType? = nil
    
        // 1
        @ObservedObject
        private var folder: ObservableSyncObject<NotesFolder>
    
        // 2
        init(folder: ObservableSyncObject<NotesFolder>) {
            self.folder = folder
        }
    
        // 3
        #if DEBUG
        init() {
            folder = ObservableSyncObject()
        }
        #endif
    
        private var notes: [SyncObjectWrapper<Note>] {
            // 1
            folder.transform(defaultValue: []) { folder in
                // 2
                folder.notes.sorted(byKeyPath: \.created, ascending: false).wrappedArray
            }
        }
    
        var body: some View {
            VStack {
                List {
                    ForEach(notes, id: \.id) { note in
                        Button(action: {
                            self.isEditing = .editNote(note)
                        }) {
                            Text(note.title)
                        }
                    }.onDelete(perform: deleteNotes)
                }.navigationBarTitle("Folder")
                    .navigationBarItems(trailing: HStack {
                        Button(action: {
                            self.isEditing = .share
                        }) {
                            Image(systemName: "person.3")
                        }.padding(.trailing)
                        Button(action: {
                            self.isEditing = .addNote
                        }) {
                            Image(systemName: "plus")
                        }
                    })
    
            }.sheet(item: $isEditing) { sheet in
                sheet.view(folder: self.folder) {
                    DispatchQueue.main.async {
                        self.isEditing = nil
                    }
                }
            }
        }
    
        func deleteNotes(indexes: IndexSet) {
            // 1
            let notesToDelete = indexes.map { notes[$0] }
            let realm = getRealm()
            try! realm.write {
                notesToDelete.forEach { note in
                    // 2
                    note.ifValid { note in
                        note.isDeleted = true
                    }
                }
            }
        }
    }
    
    extension FolderDetailView {
        enum SheetType: Identifiable {
            var id: ObjectIdentifier { .init(SheetType.self) }
    
            case addNote
            case share
            case editNote(SyncObjectWrapper<Note>)
    
            func view(folder: ObservableSyncObject<NotesFolder>, close: @escaping () -> Void) -> some View {
                // 1
                let folderObservableCopy = folder.observableCopy
                // 2
                let folder = folderObservableCopy.onDelete(call: close)
                switch self {
                case .addNote:
                    return AnyView(NoteView(folder: folder))
                case .share:
                    // 1
                    guard let group = folder.transform( { $0?.group?.observableObject() }) else {
                        return AnyView(EmptyView())
                    }
                    // 2
                    return AnyView(ShareView(group: group.onDelete(call: close)))
                case .editNote(let n):
                    let note = n.observableObject().onDelete(call: close)
                    return AnyView(NoteView(note: note, folder: folder))
                }
            }
        }
    }
    
    struct FolderDetail_Previews: PreviewProvider {
        static var previews: some View {
            NavigationView {
                FolderDetailView()
            }
        }
    }
    ```

??? note "Content of *ShareView.swift*"

    ```swift
    import SwiftUI
    import MeerkatRealmWrapper
    import MeerkatClientSyncController
    
    struct ShareView: View {
        @UserDefaultOptionalWrapper(.username)
        private var user: String?
    
        @ObservedObject
        var group: ObservableSyncObject<SyncGroup>
    
        @ObservedObject
        private var client = MeerkatClientAPI(clientConfig: Constants.clientConfig)
    
        @ObservedObject
        private var authStatus = SyncAuthStatus.shared
    
        @State
        private var username = ""
    
        @State
        private var users = [PublicGroupUser]()
    
        @State
        private var task: Cancellable? = nil
    
        @State
        private var alertType: AlertType?
    
        private var isOwner: Bool {
            group.transform { $0?.role == .owner }
        }
    
        var body: some View {
            NavigationView {
                VStack {
                    if isOwner {
                        VStack {
                            HStack {
                                Text("Add a user:")
                                Spacer()
                            }
                            TextField("Username", text: $username)
                                .modifier(FormRowModifier())
                            Button(action: add) {
                                Text("Add")
                            }.modifier(FormButtonModifier(color: .green))
                        }.padding()
                    }
                    HStack {
                        Text("Users:")
                        Spacer()
                    }.padding(.leading)
                        .padding(.bottom, -12)
                    LoadingView(isShowing: $client.isLoading, cancel: cancelTask) {
                        List {
                            // 1
                            ForEach(self.users, id: \.username) { user in
                                // 2
                                UserShareDetail(username: user.username,
                                                selectedRole: user.role,
                                                roleUpdated: self.isOwner ? self.add : nil)
                            }.onDelete(perform: self.isOwner ? self.remove : nil)
                        }
                    }
                }.alert(item: $alertType) {
                    $0.alert
                }
            }.onAppear(perform: reloadUsers)
        }
    
        func reloadUsers() {
            guard let auth = authStatus.syncConfig else {
                alertType = .notLoggedIn
                return
            }
            // 1
            group.ifValid { group in
                task = client
                    // 2
                    .getUsers(groupId: group.id, token: auth.authToken, userType: PublicGroupUser.self)
                    // 3
                    .sink(receiveCompletion: handleCompletion) { users in
                        // 4
                        self.users = users
                }
            }
        }
    
        func add() {
            add(userId: username, role: .readAndWrite)
            username = ""
        }
    
        func remove(indexes: IndexSet) {
            // 1
            guard let usersUsername = user else {
                alertType = .notLoggedIn
                return
            }
            // 2
            let usersIds = indexes.map { users[$0].username }
            // 3
            if usersIds.contains(usersUsername) {
                // 4
                alertType = .removeSelf({ self.remove(usersIds: usersIds) })
            } else {
                // 5
                remove(usersIds: usersIds)
            }
        }
    
        func remove(usersIds: [String]) {
            // 1
            guard let auth = getTokenWithGroupId() else {
                alertType = .notLoggedIn
                return
            }
            // 2
            let syncIds = usersIds.map { getSyncId(for: $0) }
            // 3
            let unsubs = syncIds.map { syncIdTask in
                syncIdTask.flatMap { syncId in
                    self.client.unsubscribe(syncId: syncId, from: auth.groupId, token: auth.token)
                }
            }
            // 4
            let merged = mergePublishers(unsubs)
            // 5
            task = taskWithReload(merged)
        }
    
        func add(userId: String, role: Role) {
            // 1
            guard let auth = getTokenWithGroupId() else {
                alertType = .notLoggedIn
                return
            }
            // 2
            let sub = getSyncId(for: userId).flatMap { syncId in
                // 3
                self.client.subscribe(syncId: syncId, to: auth.groupId, as: role, token: auth.token)
            }
            // 4
            task = taskWithReload(sub)
        }
    
        func getSyncId(for userId: String) -> AnyPublisher<String, Error> {
            // 1
            guard let auth = authStatus.syncConfig, let user = user else {
                alertType = .notLoggedIn
                // 2
                return Empty().eraseToAnyPublisher()
            }
            // 3
            return client.syncIdFor(userId: userId, interestedUserId: user, token: auth.authToken)
        }
    }
    
    extension ShareView {
        func mergePublishers<T: Publisher>(_ publishers: [T]) -> AnyPublisher<T.Output, T.Failure> {
            publishers.reduce(Empty().eraseToAnyPublisher()) { acc, elem in
                acc.merge(with: elem).eraseToAnyPublisher()
            }
        }
    
        func taskWithReload<T: Publisher>(_ task: T) -> Cancellable where T.Output == Void, T.Failure == Error {
            task
                .receive(on: DispatchQueue.main)
                .sink(receiveCompletion: handleCompletion(completion:)) {
                    self.reloadUsers()
            }
        }
    
        func handleCompletion(completion: Subscribers.Completion<Error>) {
            switch completion {
            case .finished:
                return
            case .failure(let err as BadRequest):
                alertType = .requestError(err)
            case .failure(let err):
                alertType = .other(err)
            }
        }
    }
    
    extension ShareView {
        func getTokenWithGroupId() -> (token: String, groupId: String)? {
            guard let auth = authStatus.syncConfig else {
                return nil
            }
    
            guard let groupId = group.transform(defaultValue: nil, { $0.id }) else {
                return nil
            }
    
            return (auth.authToken, groupId)
        }
    
        func cancelTask() {
            task?.cancel()
            task = nil
        }
    }
    
    extension ShareView {
        enum AlertType: Identifiable {
            var id: ObjectIdentifier { .init(Self.self) }
            case notLoggedIn
            case removeSelf(() -> Void)
            case requestError(BadRequest)
            case other(Error)
    
            var alert: Alert {
                let title: String
                let message: Text?
                switch self {
                case .removeSelf(let cb):
                    return Alert(title: Text("Do you really want to leave this group?"), primaryButton: .cancel(), secondaryButton: .destructive(Text("Leave group"), action: cb))
                case .notLoggedIn:
                    title = "User is not logged in."
                    message = nil
                case .requestError(let err):
                    title = "Network error: \(err.code)"
                    message = Text(err.body)
                case .other(let err):
                    title = "Unown error"
                    message = Text(err.localizedDescription)
                }
                return Alert(title: Text(title), message: message)
            }
        }
    }
    
    struct ShareView_Previews: PreviewProvider {
        static var previews: some View {
            ShareView(group: .init())
        }
    }
    ```
