# Setting up synchronization process

Synchronization process is responsible for integrating, notifying and handling all requests connected with synchronization.

## Application database scheme

Synchronization process is using this scheme to make semantic checks in the database so there is impossible to store integer in string property. Let's assume our application database scheme has `Note` and `NotesFolder` entity where in one `NotesFolder` multiple instances of `Note` can be stored.

![Notes scheme](../img/notesScheme.png)

### Define scheme protocols

At first we define protocols for our scheme and then we create some structs which will conforms them. Create a new file named *SyncScheme.swift* in *Models* directory with following content:

```swift
import MeerkatSchemeDescriptor
import Foundation

public protocol NoteScheme: SchemeDescribable {
    var title: String { get }
    var text: String { get }
    var created: Date { get }
}

public protocol NotesFolderScheme: SchemeDescribable {
    var title: String { get }
    var notes: Notes { get }

    associatedtype Notes: Collection where Notes.Element: NoteScheme
}
```

!!! note "Protocols? Why?"

    We could simply create structs without defining these protocols. But your scheme is needed in server code and client application code as well. This way you can use same protocols in both projects and make sure your server and clients are using same scheme.
    
Both protocols conform to `#!swift SchemeDescribable`. This protocol is responsible to create a scheme for given type. Note that protocol requires that conforming type needs to be initializable without arguments (`#!swift init()`).
    
??? info "Why `#!swift associatedtype`?"

    This way we are able to use this protocol without strictly defining collection type. In our server application we can use `#!swift Array` type but in our client's application we have to use `#!swift List` type.

### Implement scheme

Now we create two structs with protocol conformance to created protocols. In *Models* directory create file a *Note.swift* with content:

```swift
import Foundation

struct Note: NoteScheme {
    let title = ""

    let text = ""

    let created = Date()
}
```

and *NotesFolder.swift* with content:

```swift
struct NotesFolder: NotesFolderScheme {
    let title: String = ""

    let notes: Notes = []

    typealias Notes = [Note]
}
```

??? question "Can I use `#!swift var` instead of `#!swift let`?"

    Ofcourse :) You can add new `#!swift init` function or use a `#!swift class` instead. But if you add another property it became part of scheme. But don't worry :) you can implement `#!swift schemeIgnoredProperties` property to ignore them.
    
    ```swift 
    class NotesFolder: NotesFolderScheme {
        var title: String
    
        var notes: Notes = []
    
        let myIgnoredProperty: Bool
    
        required init() {
            title = "unknown"
            myIgnoredProperty = true
        }
    
        init(title: String = "", prop: Bool = false) {
            self.title = title
            myIgnoredProperty = prop
        }
    
        typealias Notes = [Note]
    
        static var schemeIgnoredProperties: [String] {
            ["myIgnoredProperty"]
        }
    }
    ```

### Scheme registration

Open *configure.swift* file and after provider registration add following:

```swift
let scheme = [Note.self, NotesFolder.self].scheme
services.register(scheme)
```

Thanks to `#!swift SchemeDescribable` protocol all types are inferred and our scheme is ready to go.

## Notificators

Notitificators are used to notify user's device about possible updates in the database. In this case we use `EmptyNotificator` which means our application will be working without notifications.

Add following code after scheme registration:

```swift
services.register(EmptyNotificator.self)
```

## Inner models registration

Synchronization process is using multiple private models and we need to set up their migration so they can be dynamically created in our database. This can be done by calling `#!swift meerkatProvider.setUpMigration(_:)` function with our migrations. Right before migration registration call this function so your migration code should look similar to this:

```swift
var migrations = MigrationConfig()
PrivateUser.defaultDatabase = .mysql
UserToken.defaultDatabase = .mysql
migrations.add(model: PrivateUser.self, database: .mysql)
migrations.add(model: UserToken.self, database: .mysql)
meerkatProvider.setUpMigration(&migrations) // add inner model's migrations
services.register(migrations)
```

## Add synchronization routes

MeerkatSync provide default implementation for routes to allow synchronization. After `router` init add `#!swift meerkatProvider.setUpRoutes(router)` so it be similar to following:

```swift
let router = EngineRouter.default()
meerkatProvider.setUpRoutes(router)
try routes(router)
services.register(router, as: Router.self)
```

This code will register six new routes:

| Method   |      Route      |  Info |
|----------:|:-------------|------|
| **PATCH** |  */sync/:sid* | Accepts new updates from a client with sync id `sid` |
| **POST** |    */sync/:sid*   | Accepts pull requests from a user with sync id `sid` |
| **POST** | */groups/:gid* | Add group with id `gid` |
| **DELETE** | */groups/:gid* | Remove group with id `gid` |
| **POST** | */groups/:gid/subscribers/uid* | Add new subscriber with id `uid` into group `gid` |
| **DELETE** | */groups/:gid/subscribers/uid* | Remove subscriber with id `uid` grom group `gid` |    

## Build & run

Try to build and run your server application. Your app should start without complications. Now open your database and you should see new tables with `sync_` prefix. All these tables are used to store synchronization information.

## Summary

Your synchronization process is ready to use, but we need to add way to create some users. We will cover this in next page.


??? note "Content of *configure.swift*"

    Content of your *configure.swift* file should be similar to this:

    ```swift
    import MeerkatServerProvider

    /// Called before your application initializes.
    public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
        // Register providers first
        let meerkatProvider = MeerkatServerProvider(userType: PrivateUser.self)
        try services.register(meerkatProvider)
        try services.register(AuthenticationProvider())
    
        let scheme = [Note.self, NotesFolder.self].scheme
        services.register(scheme)
    
        services.register(EmptyNotificator.self)
    
        // Register routes to the router
        let router = EngineRouter.default()
        meerkatProvider.setUpRoutes(router)
        try routes(router)
        services.register(router, as: Router.self)
    
        // Register middleware
        var middlewares = MiddlewareConfig() // Create _empty_ middleware config
        // middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
        middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
        services.register(middlewares)
    
        // insert database configuration
        let hostname =  // maybe "127.0.0.1"?
        let port =      // maybe 3306?
        let username =  // maybe "root"?
        let password: String? =  // maybe nil?
        let database = 
    
        let config = MySQLDatabaseConfig(hostname: hostname, port: port, username: username, password: password, database: database)
        let db = MySQLDatabase(config: config)
        var databases = DatabasesConfig()
        databases.add(database: db, as: .mysql)
        services.register(databases)
    
        // insert migrations
        var migrations = MigrationConfig()
        PrivateUser.defaultDatabase = .mysql
        UserToken.defaultDatabase = .mysql
        migrations.add(model: PrivateUser.self, database: .mysql)
        migrations.add(model: UserToken.self, database: .mysql)
        meerkatProvider.setUpMigration(&migrations) // add inner model's migrations
        services.register(migrations)
    }
    ```
