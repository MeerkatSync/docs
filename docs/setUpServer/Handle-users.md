# Handle users

MeerkatSync provides an easy method to implement user registrations with token authentication support. For this we need to implement some type that conforms to `#!swift UserBuilderProtocol` protocol. 

## UserBuilderProtocol

This protocol defines how to register a new user and how to create new tokens.

### Associated type

To ensure type safety, protocol has four associated types. Let's take a look at them:

- `UserTable` - Model of a user (in our case `PrivateUser`).
- `PublicUser` - Type with attributes required in registration such as username, password, age, etc...
- `UserTokenAuth` - Type with attributes required to generate a new token for a user.
- `UserDevice` - Type with attributes required to register a new device for a user.

### Functions

Protocol has three required functions:

- `#!swift createUser(from:on:)` In this function we create a new user.
- `#!swift createToken(for:on:)` In this function we create a new token for a user.
- `#!swift createToken(for:on:)` (overloaded) In this function we create a new token for a newly registered user.

## Implement your builder

Now we are going to implement your custom builder.

### User registration information

Create a new file named *PublicUserInformation.swift* in the *Models* directory with the following content:

```swift
import MeerkatServerProvider

struct PublicUserInformation: Content {
    let username: String
    let password: String
}
```

This structure represents data required to register a new user. In our case we expect username and password.

!!! warning

    Real application requires more information! :)

### UserBuilder

Create a new file *UserBuilder.swift* in the *Models* directory with the following content:

```swift
import MeerkatServerProvider

struct UserBuilder: UserBuilderProtocol {
    typealias UserTable = PrivateUser
    typealias PublicUser = PublicUserInformation
    typealias UserTokenAuth = PublicUserInformation // 1
    typealias UserDevice = DefaultDevice            // 2
    
    let userTableId: KeyPath<PrivateUser, String> = \.username // 3
}
```

1. We use same attributes (username, password) to create a new token as we used for the registration.
2. MeerkatSync provides default implementation for a device.
3. Keypath to unique attribute with `#!swift String` type

Now we need to implement a function to create a new user. Add this function into your `UserBuilder`.

```swift
func createUser(from user: PublicUserInformation, on container: Container) throws -> EventLoopFuture<PrivateUser> {
    // 1
    let password = try container.make(BCryptDigest.self).hash(user.password)
    // 2
    let newUser = PrivateUser(username: user.username, password: password)
    // 3
    let userNotExists = container.withPooledConnection(to: .mysql) { conn -> Future<Void> in
        PrivateUser.query(on: conn)
            .filter(\.username == newUser.username)
            .first()
            .map { optU -> Void in
                if let u = optU {
                    // 4
                    throw DBWrapperError.userAlreadyExists(id: u.username)
                }

        }
    }
    // 5
    let save = userNotExists.then {
        container.withPooledConnection(to: .mysql) {
            newUser.save(on: $0)
        }
    }
    return save
}
```

1. For security reasons we need to store only hash of user's password.
2. Create a new user.
3. Database query to check if there is any user with the same username as registering user. 
4. Query result containing user with the same id. We need to throw an error to prevent the registration.
5. If `userNotExists` ends successfuly we can save new user to our database.
    
    
And add these functions too:

```swift
func createToken(for user: PublicUserInformation, on container: Container) -> EventLoopFuture<UserToken> {
    // 1
    let getUser = container.withPooledConnection(to: .mysql) { conn in
        PrivateUser.query(on: conn)
            .filter(\.username == user.username)
            .first()
            .unwrap(or: DBWrapperError.unknownUser(id: user.username)) }
    // 2
    let isPasswordValid = getUser.map(to: Void.self) {
        // 3
        let check = try container.make(BCryptDigest.self)
            .verify(user.password, created: $0.passwordHash)
        // 4
        guard check else { throw Abort(.unauthorized) }
        return ()
    }
    // 5
    return isPasswordValid.transform(to: getUser).flatMap {
        try self.createToken(for: $0, on: container)
    }
}

// 6
func createToken(for user: PrivateUser, on container: Container) throws -> EventLoopFuture<UserToken> {
    container.withPooledConnection(to: .mysql) { conn in
        UserToken(token: self.randomString(length: 128), userID: user.id!).save(on: conn)
    }
}
```

1. Database query to find user by username.
2. Check if given password is valid. If password is invalid we throw an error, otherwise we return `#!swift ()`.
3. Verify password hash.
4. Verification must be true, otherwise password is incorrect.
5. Create a new token for a user if password is valid.
6. Create and store new token (in real application you want it to be unique, in this project it is random string with 128 characters).


!!! note

    You can do multiple validations in these functions (age, email, ...).
    

!!! warning "Thread safety"

    These functions are not thread safe! In real application you will need mutex.

Your user builder is finished and we can add some routes. :)

## Register user routes

Open the *routes.swift* file and in `#!swift routes(_:)` function add following:

```swift
let userController = DefaultUserController(builder: UserBuilder())
userController.register(on: router)
```

Now your web application can handle requests with following routes:

| Method   |      Route      |  Info |
|----------:|:-------------|------|
| **POST** |  */users* | Create new user |
| **DELETE** |  */users/:uid* | Delete user with id `uid` (token protected)|
| **POST** |  */users/tokens* | Create new token for user |
| **GET** |  */users/:uid/sync* | Get sync id for user with id `uid` (token protected)|
| **POST** |    */users/:uid/devices*   | Add new device for user with id `uid` (token protected) |
| **DELETE** |    */users/:uid/devices/:did*   | Delete device with id `did` owned by user with id `uid` (token protected) |

??? question "Can I use different routes and handling?"

    Yes you can! You don't have to create user builder and register `DefaultUserController`. Just don't forget to register sync id and user's groups when you create new user :) This can be done by calling this code in your request handler: 
    
    ```swift
    let sync = try req.make(ServerSyncController.self)
    let createUserAndGroup = sync.createUser(with: user.syncId).then { u in
        sync.createGroup(with: user.syncId, objects: [], by: u)
    }
    ```

## Register group information routes

There are usecases where you want to allow users acces to list of all users in their groups or access to sync id of different users. Luckily MeerkatSync has `PublicGroupInfoController` for this purpose.

### Public information about users in a group

First we need to define struct with public information about users in a group.

In the *Models* directory create a new file *PublicGroupUser* with the following content:

```swift
import MeerkatServerProvider

struct PublicGroupUser: Content {
    let username: String
    let role: Role
}
```

We will provide username and role for every member of the group

### Register routes

Register new routes in the *routes.swift* file with the following code:

```swift 
let groupInfoController = PublicGroupInfoController(userIdKeyPath: \PrivateUser.username) { user, role, groupId in
    PublicGroupUser(username: user.username, role: role)
}
groupInfoController.register(on: router)
```

We had to specify keypath to a unique attribute of our user model in the first argument. Second argument (closure) creates public information from our private user and its role.


Two new routes were added to your application.

| Method   |      Route      |  Info |
|----------:|:-------------|------|
| **GET** |  */groups/:gid/users/* | Get all users in group with id `gid` (token protected) |
| **GET** |  */public-users/:tid/sync-id/:uid* | Get sync id for user with id `uid`. Request is made by user with id `tid`. (token protected) |

## Build & run

Run the application, open terminal and check if there is curl installed with `#!sh curl --version`. If you have `#!sh curl` you can check if your app is working. 

### Create new user

You can create a new user by calling following command:

```sh
curl -i --header "Content-Type: application/json" \
  --request POST \
  --data '{"username":"Mark","password":"aaaa"}' \
  http://localhost:8080/users
```

Your output should be similar to this.

```
HTTP/1.1 200 OK
content-type: application/json; charset=utf-8
content-length: 188
date: Fri, 24 Apr 2020 09:12:59 GMT
Connection: keep-alive

{"token":"Rp7ZwVDnjk76O6LUBPVMCkJQnp6NFjd9c3hcpsvXFyq2FcWleSeC0HpOhCS6umcnvdaRRGQ2W5UypvKB2wLj8IsdvOK3vSgFf1ukOpZKrE7SuAoO884QCtfgKHfjnbXX","syncId":"C3C553AA-F6D6-436B-885E-75B72BBA601E"}
```

This command registers new user with username *Mark* and password *aaaa*. When you open your database you will see new records in *sync_users*, *PrivateUser*, *UserToken*, *sync_groups* and *sync_roles* tables.

!!! warning "Your output will be different!"

    Value of sync id will be different. Don't worry this is a good thing ;).

### Create new token

Now let's create a new token for *Mark*. Run this command:

```sh
curl -i --header "Content-Type: application/json" \
  --request POST \
  --data '{"username":"Mark","password":"aaaa"}' \
  http://localhost:8080/users/tokens
```

output: 

```
HTTP/1.1 200 OK
content-type: application/json; charset=utf-8
content-length: 140
date: Fri, 24 Apr 2020 09:23:34 GMT
Connection: keep-alive

{"token":"a5ySGjR1iMgDdYZTFXTzkqod0re9dODwvf04hBqUOYtXj4kEs2Wq7pbzsZK4r3RFEo90rRorsG8rD3Nnp5BY1Virx33wASGvZ5MyJ0ZEKM9hFvTjcAfh0cBFUXrsm3KG"}
```

!!! note 

    Your token value will be different too :).
    
In your database there is a new record in the *UserToken* table.

### Delete user

Now we try to delete our user. Because of required token authentification we need to provide token in the request. Run the following code, but **replace `YOUR_TOKEN` with your actual token** value returned in the previous command.

```sh
BEARER_TOKEN="YOUR_TOKEN"
```

```sh
curl -i --header "Content-Type: application/json" \
    --header "Authorization: Bearer $BEARER_TOKEN" \
  --request DELETE \
  http://localhost:8080/users/Mark
```

output:
```
HTTP/1.1 200 OK
content-length: 0
date: Fri, 24 Apr 2020 09:51:21 GMT
Connection: keep-alive
```

Now you can check your database tables again and you will notice they are empty :).

## Summary

Your server is ready to go :) Let's start with the client :)

??? note "Content of *UserBuilder.swift*"

    Content of your *UserBuilder.swift* file should be similar to this:

    ```swift
    import MeerkatServerProvider
    
    struct UserBuilder: UserBuilderProtocol {
        typealias UserTable = PrivateUser
        typealias PublicUser = PublicUserInformation
        typealias UserTokenAuth = PublicUserInformation // 1
        typealias UserDevice = DefaultDevice            // 2
    
        let userTableId: KeyPath<PrivateUser, String> = \.username // 3
    
        func createUser(from user: PublicUserInformation, on container: Container) throws -> EventLoopFuture<PrivateUser> {
            // 1
            let password = try container.make(BCryptDigest.self).hash(user.password)
            // 2
            let newUser = PrivateUser(username: user.username, password: password)
            // 3
            let userNotExists = container.withPooledConnection(to: .mysql) { conn -> Future<Void> in
                PrivateUser.query(on: conn)
                    .filter(\.username == newUser.username)
                    .first()
                    .map { optU -> Void in
                        if let u = optU {
                            // 4
                            throw DBWrapperError.userAlreadyExists(id: u.username)
                        }
    
                }
            }
            // 5
            let save = userNotExists.then {
                container.withPooledConnection(to: .mysql) {
                    newUser.save(on: $0)
                }
            }
            return save
        }
    
        func createToken(for user: PublicUserInformation, on container: Container) -> EventLoopFuture<UserToken> {
            // 1
            let getUser = container.withPooledConnection(to: .mysql) { conn in
                PrivateUser.query(on: conn)
                    .filter(\.username == user.username)
                    .first()
                    .unwrap(or: DBWrapperError.unknownUser(id: user.username)) }
            // 2
            let isPasswordValid = getUser.map(to: Void.self) {
                // 3
                let check = try container.make(BCryptDigest.self)
                    .verify(user.password, created: $0.passwordHash)
                // 4
                guard check else { throw Abort(.unauthorized) }
                return ()
            }
            // 5
            return isPasswordValid.transform(to: getUser).flatMap {
                try self.createToken(for: $0, on: container)
            }
        }
    
        // 6
        func createToken(for user: PrivateUser, on container: Container) throws -> EventLoopFuture<UserToken> {
            container.withPooledConnection(to: .mysql) { conn in
                UserToken(token: self.randomString(length: 128), userID: user.id!).save(on: conn)
            }
        }
    }
    ```


??? note "Content of *routes.swift*"

    Content of your *routes.swift* file should be similar to this:

    ```swift
    import MeerkatServerProvider

    /// Register your application's routes here.
    public func routes(_ router: Router) throws {
        let helloController = HelloWorldController()
        router.get(use: helloController.index)
    
        let userController = DefaultUserController(builder: UserBuilder())
        userController.register(on: router)
    
        let groupInfoController = PublicGroupInfoController(userIdKeyPath: \PrivateUser.username) { user, role, groupId in
            PublicGroupUser(username: user.username, role: role)
        }
        groupInfoController.register(on: router)
    }
    ```
