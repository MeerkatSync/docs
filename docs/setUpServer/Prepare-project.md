# Prepare the project

We'll use [Vapor](https://vapor.codes) framework to build a server. Open https://docs.vapor.codes/3.0/ and install all the requirements such as the Swift compiler or Xcode.


??? info "Completed project from this tutorial"

    You can download completed project from its [repository](https://gitlab.com/MeerkatSync/templates/servertutorialcompleted).

## Download template

Clone a [template](https://gitlab.com/MeerkatSync/templates/servertutorialtemplate) for the server, `cd` into it and try to build the project (`#!sh swift build` or use shortcut ++cmd+b++ in Xcode). If your project builds successfully you are ready to continue. If there is any error, check your swift version (`#!sh swift --version` should print 5.1 or newer).

After successful build, try to run the project with `#!sh swift run` or tap at the play icon in Xcode (Xcode shortcut is ++cmd+r++). Output in console should be similar to:

```
Server starting on http://localhost:8080
```

If so, open this url in your favorite browser and you should see:

```
It works!
```

Now we are ready to start!

## What's inside my project?

First you need to get familiar with the project structure to orientate in your new project.

### Package.swift

This file contains receipt how to build your project. It contains dependencies, targets and products. At this time your project has only one dependency (MeerkatServerProvider) which contains more dependencies (Vapor, MeerkatMySQL and so on...).

??? note "Adding new dependency"

    Add new package into `dependencies` and name of the package add as dependency to target that depends on new package.


### App directory

With high probability all your code will be in *Sources/App* directory. It contains all important code of your application.

- *app.swift* - contains all code responsible to create your application, probably you will not interact with this file.
- *configure.swift* - contains configuration for your application. This file contains configuration function `configure` with some middlewares and route engine responsible for routing.
- *routes.swift* - contains function where you can add custom routes and controllers for you application.
- *boot.swift* - contains function called right after the boot of your application.

### Controllers

You can group similar code into controllers in the *Sources/App/Controllers* directory. Template program contains only one controller. 

### Models

Model mostly refers to database used by your application. All models should be stored in the *Sources/App/Models* directory.

