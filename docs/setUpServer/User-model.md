# Set up users and authentication

You need to store all users into your database. This project uses [MySQL](https://www.mysql.com) database, so you need to install one.

## Database configuration

We need to set database configuration in *configure.swift* file. Open it and paste following content under `#!swift // insert database configuration` comment:

```swift
let hostname = <#YOUR HOSTNAME#> // maybe "127.0.0.1"?
let port = <#YOUR PORT#>         // maybe 3306?
let username = <#YOUR USERNAME#> // maybe "root"?
let password: String? = <#YOUR PASSWORD#> // maybe nil?
let database = <#YOUR DATABASE NAME#>

let config = MySQLDatabaseConfig(hostname: hostname, port: port, username: username, password: password, database: database)
let db = MySQLDatabase(config: config)
var databases = DatabasesConfig()
databases.add(database: db, as: .mysql)
services.register(databases)
```

??? info "Using local MySQL database?"

    If you are using local mysql database and root user you can replace:
    
    ```swift 
    let config = MySQLDatabaseConfig(hostname: hostname, port: port, username: username, password: password, database: databse)
    ```
    
    with:
    
    ```swift
    let config = MySQLDatabaseConfig.root(database: database)
    ```

## User

For every user we store his username, password hash and synchronization id. 

Create a new file *PrivateUser.swift* in *Models* directory with following content:

```swift
import MeerkatServerProvider // 1

final class PrivateUser: MySQLModel, PrivateUserTable { // 2
    var id: Int?
    var username: String
    var passwordHash: String
    var syncId: String = UUID().uuidString

    init(username: String, password: String) {
        self.username = username
        self.passwordHash = password
    }
}

extension PrivateUser: TokenAuthenticatable { // 3
    typealias TokenType = UserToken
}

extension PrivateUser: Migration { } // 4

```

1. Imports all important code and libraries
2. Add protocol requirements. Thanks to `#!swift MySQLModel` we can store instances of this class in MySQL database. `#!swift PrivateUserTable` protocol requires `#!swift syncId` property and protocol conformance is important to allow synchronization process.
3. Connect this class with token authentication (we create `#!swift UserToken` class in next step). 
4. Your application needs to know how to migrate this class into database scheme. Thanks to default implementation of `#!swift Migration` protocol you don't need to write anything but this conformance.

??? info "Why is there synchronization id?"

    Synchronization id represents unique identifier of user in all synchronization processes. For this reason we use UUID because it is pretty unique[^1].

## Token

The token is used to authenticate to prevent sending private information (password...) to server. Create a new file *UserToken.swift* in *Models* with following content:

```swift 
import MeerkatServerProvider

struct UserToken: MySQLModel {
    var id: Int?
    var token: String   // 1
    var userID: PrivateUser.ID // 2
}

extension UserToken: Token {
    typealias UserType = PrivateUser // 3

    static var tokenKey: WritableKeyPath<UserToken, String> {
        return \.token  // 4
    }

    static var userIDKey: WritableKeyPath<UserToken, PrivateUser.ID> {
        return \.userID  // 5
    }
}

extension UserToken: Migration { }
```

1. Actual value of the token.
2. Token's owner identifier.
3. Owners type.
4. Keypath to the token value.
5. Keypath to property with owner's identifier.


## Configure migration

When our user and token model is completed we need to set migrations for them in `configure` function. Under `#!swift // insert migrations` comment insert:

```swift
var migrations = MigrationConfig()
PrivateUser.defaultDatabase = .mysql
UserToken.defaultDatabase = .mysql
migrations.add(model: PrivateUser.self, database: .mysql)
migrations.add(model: UserToken.self, database: .mysql)
services.register(migrations)
```


## Add MeerkatServerProvider

`#!swift MeerkatServerProvider` register all importand services used within synchronization process. Right at the begining of `configure` function insert following:

```swift
let meerkatProvider = MeerkatServerProvider(userType: PrivateUser.self)
try services.register(meerkatProvider)
```

## Add authentication provider

To enable authentication insert following code after `#!swift try services.register(meerkatProvider)`:

```swift
try services.register(AuthenticationProvider())
```




## Try build & run!

At this time your application should build and you can run your application. Write `#!sh swift run` or hit ++cmd+r++ if you are using Xcode. 

Now open your database and you should see three new tables:

- PrivateTable
- UserToken
- fluent

## Summary

At this time your application can connect to database and work with user and token model. In next page we create scheme and start our synchronization process.

??? note "Content of *configure.swift*"

    Content of your *configure.swift* file should be similar to this:

    ```swift
    import MeerkatServerProvider

    /// Called before your application initializes.
    public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
        // Register providers first
        let meerkatProvider = MeerkatServerProvider(userType: PrivateUser.self)
        try services.register(meerkatProvider)
        try services.register(AuthenticationProvider())
    
        // Register routes to the router
        let router = EngineRouter.default()
        try routes(router)
        services.register(router, as: Router.self)
    
        // Register middleware
        var middlewares = MiddlewareConfig() // Create _empty_ middleware config
        // middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
        middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
        services.register(middlewares)
    
        // insert database configuration
        let hostname =  // maybe "127.0.0.1"?
        let port =      // maybe 3306?
        let username =  // maybe "root"?
        let password: String? =  // maybe nil?
        let database = 
    
        let config = MySQLDatabaseConfig(hostname: hostname, port: port, username: username, password: password, database: database)
        let db = MySQLDatabase(config: config)
        var databases = DatabasesConfig()
        databases.add(database: db, as: .mysql)
        services.register(databases)
    
        // insert migrations
        var migrations = MigrationConfig()
        PrivateUser.defaultDatabase = .mysql
        UserToken.defaultDatabase = .mysql
        migrations.add(model: PrivateUser.self, database: .mysql)
        migrations.add(model: UserToken.self, database: .mysql)
        services.register(migrations)
    }
    ```

[^1]: UUID can produce collitions https://softwareengineering.stackexchange.com/questions/130261/uuid-collisions 
